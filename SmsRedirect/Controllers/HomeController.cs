﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace SmsRedirect.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RedirectToLong(string shortURL)
        {
            if (string.IsNullOrEmpty(shortURL))
                return RedirectToAction("NotFound", "Home");
            else
            {
                string url = GetRealLink(shortURL);

                if (string.IsNullOrEmpty(url) || url.IndexOf("failed:") >= 0)
                    return RedirectToAction("NotFound", "Home");
                else
                {
                    return Redirect(HttpUtility.UrlDecode(url)); // redirects to the long URL
                }
            }
        }

        public ActionResult NotFound()
        {
            return View();
        }

        private string GetRealLink(string hash)
        {
            string dbcon = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string result = "";
            SmsEvent sms = new SmsEvent();
            try
            {
                using (SqlConnection adoConn = new SqlConnection(dbcon))
                {
                    DataSet ds = new DataSet();
                    adoConn.Open();
                    string sqlcmd = @"select ID,url,ClientID,CampaignID,EventID,CSID from Redirects where hash='" + hash + @"'";
                    SqlDataAdapter command = new SqlDataAdapter(sqlcmd, adoConn);
                    command.Fill(ds, "ds");
                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            sms.ID = dt.Rows[0]["ID"].ToString();
                            result = dt.Rows[0]["url"].ToString();
                            sms.ClientId = dt.Rows[0]["ClientID"].ToString();
                            sms.CampaignId = dt.Rows[0]["CampaignID"].ToString();
                            sms.EventId = dt.Rows[0]["EventID"].ToString();
                            sms.CsId = dt.Rows[0]["CSID"].ToString();
                            sms.Url = HttpUtility.UrlDecode(result);
                        }
                    }

                    string logResult = "Redirect";
                    if (string.IsNullOrEmpty(result))
                    {
                        logResult = "Link not found";
                    }
                    string sqlLog = string.Format("INSERT INTO RedirectLog(TimeStamp,URL,Hash,Result) Values(GETDATE(),'{0}','{1}','{2}');", result, hash, logResult);
                    if (!string.IsNullOrEmpty(sms.ID))
                    {
                        sqlLog += "UPDATE Redirects SET Used=1 where ID=" + sms.ID;
                    }
                    using (var cmd = new System.Data.SqlClient.SqlCommand(sqlLog, adoConn))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    if (!string.IsNullOrEmpty(sms.ClientId)) {
                        Task t = new Task(() => AddActionLog(sms));
                        t.Start();
                    }
                    
                }
                return result;
            }
            catch (Exception ex)
            {
                return "failed: " + ex.Message;
            }
        }

        private void AddActionLog(SmsEvent sms)
        {
            string dbname = GetClientDB(sms.ClientId);
            if (!string.IsNullOrEmpty(dbname) && dbname.IndexOf("failed:") < 0)
            {
                try
                {
                    string savetoactionlog = GetConfigString("SaveToActionLog");
                    string dbcon = string.Format(ConfigurationManager.ConnectionStrings["ClientDBConnection"].ConnectionString, dbname);
                    string sqlLog = "";
                    if (savetoactionlog == "old")
                        sqlLog = "INSERT INTO actionlog ([CustomerID],[ActivityTypeID],[ActionTypeID],[CampaignID],[EventID],[URL]) VALUES(@CustomerID,2,26,@CampaignID,@EventID,@URL)";
                    else if (savetoactionlog == "new")
                        sqlLog = "INSERT INTO actionlog_sms ([CustomerID],[ActivityTypeID],[ActionTypeID],[CampaignID],[EventID],[URL]) VALUES(@CustomerID,2,26,@CampaignID,@EventID,@URL)";
                    else
                    {
                        sqlLog = "INSERT INTO actionlog ([CustomerID],[ActivityTypeID],[ActionTypeID],[CampaignID],[EventID],[URL]) VALUES(@CustomerID,2,26,@CampaignID,@EventID,@URL)";
                        sqlLog += "INSERT INTO actionlog_sms ([CustomerID],[ActivityTypeID],[ActionTypeID],[CampaignID],[EventID],[URL]) VALUES(@CustomerID,2,26,@CampaignID,@EventID,@URL)";
                    }

                    using (SqlConnection adoConn = new SqlConnection(dbcon))
                    {
                        using (var cmd = new System.Data.SqlClient.SqlCommand(sqlLog, adoConn))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = sms.CsId;
                            cmd.Parameters.Add("@CampaignID", SqlDbType.Int).Value = sms.CampaignId;
                            cmd.Parameters.Add("@EventID", SqlDbType.Int).Value = sms.EventId;
                            cmd.Parameters.Add("@URL", SqlDbType.VarChar, 200).Value = sms.Url;
                            adoConn.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private string GetClientDB(string clientId)
        {
            string dbcon = ConfigurationManager.ConnectionStrings["IvoDbConnection"].ConnectionString;
            string result = "";
            try
            {
                using (SqlConnection adoConn = new SqlConnection(dbcon))
                {
                    DataSet ds = new DataSet();
                    string sqlcmd = @"select dbName from UserToDB where UID=" + clientId;
                    SqlDataAdapter command = new SqlDataAdapter(sqlcmd, adoConn);
                    command.Fill(ds, "ds");
                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            result = dt.Rows[0]["dbName"].ToString();
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                return "failed: " + ex.Message;
            }
        }

        private string GetConfigString(string key)
        {
            object objModel = new object();
            try
            {
                objModel = ConfigurationManager.AppSettings[key];
            }
            catch { }

            return objModel.ToString();
        }

    }

    public class SmsEvent
    {
        public string ID { get; set; }
        public string ClientId { get; set; }
        public string CampaignId { get; set; }
        public string EventId { get; set; }
        public string CsId { get; set; }
        public string Url { get; set; }
    }
}
